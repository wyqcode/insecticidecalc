# -*- coding: utf-8 -*-

import os
import os.path

# dir = os.getcwd()
dir = "./"


def findfile(dirPath):
	uiList = []
	moduleList = []
	for relpath, dirs, files in os.walk(dirPath):
		for filename in files:
			if os.path.splitext(filename)[1] == '.ui':
				full_path = os.path.join(dir, relpath, filename)
				uiList.append(os.path.normpath(os.path.abspath(full_path)))

				module_path = os.path.join(relpath, filename)
				module_name = os.path.splitext(module_path)[0].replace('./', '').replace('/', '.')
				moduleList.append(module_name)
	return uiList, moduleList


# py name
def transPyFile(filename):
	return os.path.splitext(filename)[0] + '.py'


# Transform ui to py
def runMain():
	list, moduleList = findfile(dir)
	for uifile in list:
		pyfile = transPyFile(uifile)
		cmd = 'pyuic5 -o {pyfile} {uifile}'.format(pyfile=pyfile, uifile=uifile)
		os.system(cmd)
	return moduleList


if __name__ == "__main__":
	importList = runMain()
