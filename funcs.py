# -*- coding: utf-8 -*-
import numpy as np
from pprint import pprint


class Logistic:

    def __init__(self, D, P, N):
        if type(D) is not list or type(P) is not list or type(N) is not list:
            raise SystemExit
        elif len(D) != len(P) != len(N):
            raise SystemExit
        self.D = np.array(D)
        self.P = np.array(P)
        self.N = np.array(N)

    def calcfun(self):

        X = np.log10(self.D)
        # print(X)
        L = np.log(np.true_divide(1, np.true_divide(1, self.P) - 1))
        print("L:",L)
        Omga = self.P * (1 - self.P)
        f0 = self.N * Omga  # n*w
        print("f0:",f0)
        f1 = f0.sum()  # sum(n*w)
        f01 = f0 * X
        print(f01*X)
        f2 = f01.sum()  # sum(n*w*x)
        f02 = f0 * L
        print(f01*L)
        f3 = f02.sum()  # sum(n*w*l)
        f4 = (f01 * X).sum()  # sum(n*w*x*x)
        f5 = (f02 * L).sum()  # sum(n*w*l*l)
        f6 = (f01 * L).sum()  # sum(n*w*x*l)

        SP = f6 - np.true_divide(f2 * f3, f1)
        # print(SP)
        SSx = f4 - np.true_divide(f2 * f2, f1)
        # print(SSx)
        SSl = f5 - np.true_divide(f3 * f3, f1)
        # print(SSl)

        b = np.true_divide(SP, SSx)
        # print('b:',b)
        a = np.true_divide((f3 - b * f2), f1)
        # print('a:',a)
        r = np.true_divide(SP, (np.sqrt(SSx * SSl)))
        # print('r:',r)
        xt = X.mean()
        lt = X.mean()

        mt = xt - lt * (np.true_divide(SP, SSl))
        # print('mt:',mt)
        LD50 = pow(10, mt)
        # print('LD50:',LD50)
        m1 = np.true_divide(1, f1)
        m2 = np.true_divide(np.square(mt - xt), SSx)
        S10mt = np.true_divide(m1 + m2 , b)
        # print('S10mt:',S10mt)
        lr = a + b * X
        # print('lr:',lr)

        sum = LD50 + S10mt
        minus = LD50 - S10mt

        # print('LD50 + S10mt:',sum)
        # print('LD50 - S10mt:',minus)
        result = {
            "a": a,
            "b": b,
            "r": r,
            "ssx":SSx,
            "ssl":SSl,
            "sp":SP,
            "mt": mt,
            "LD50":LD50,
            "S10mt":S10mt,
            "LD50+S10mt":sum,
            "LD50-S10mt":minus,
            "process:":{
                "d":self.D,
                "n": self.N,
                "p": self.P,
                "w":Omga,
                "l":L,
                "f0":f0,
                "f1":f1,
                "f2":f2,
                "f3":f3,
                "f4":f4,
                "f5":f5,
                "f6":f6,
            }
        }
        pprint(result)
        return result


def main():
    d = [1.275, 1.63, 1.94, 2.2, 2.47]
    p = [0.1818, 0.4754, 0.566, 0.8361, 0.8833]
    n = [55, 61, 53, 61, 60]
    calc = Logistic(D=d, P=p, N=n)
    calc.calcfun()


if __name__ == "__main__":
    main()